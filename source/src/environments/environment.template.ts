export const environment = {
    apiBaseUrl: '${TCHALLENGE_API_BASE_URL}',
    clientBaseUrl: '${TCHALLENGE_CLIENT_URL}',
    production: true
};