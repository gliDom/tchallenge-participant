export const environment = {
    apiBaseUrl: 'https://tfrontend-dev.openshift.devops.t-systems.ru/api',
    clientBaseUrl: 'https://tfrontend-dev.openshift.devops.t-systems.ru/',
    production: false
};
 