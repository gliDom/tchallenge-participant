export const environment = {
    apiBaseUrl: 'https://tfrontend.openshift.devops.t-systems.ru/api',
    clientBaseUrl: 'https://tfrontend.openshift.devops.t-systems.ru',
    production: true
};
