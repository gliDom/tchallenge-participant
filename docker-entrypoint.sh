#!/bin/sh
envsubst < /usr/app/src/environments/environment.template.ts > /usr/app/src/environments/environment.ts
if ! [ -d /usr/app/node_modules ]; then npm install; fi
npm run start -- --port 80 --host 0.0.0.0 --disableHostCheck true
