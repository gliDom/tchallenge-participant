#FROM node:14.15.3-alpine3.11 as builder
#ARG apiBaseUrl=http://localhost:4567
#ARG clientBaseUrl=http://localhost:4200
#ENV TCHALLENGE_API_BASE_URL=apiBaseUrl
#ENV TCHALLENGE_CLIENT_URL=clientBaseUrl
#RUN apk --no-cache add gettext
#RUN mkdir /usr/app/
#COPY source/ /usr/app/
#COPY docker-entrypoint.sh /usr/app/
#WORKDIR /usr/app/
#RUN if ! [ -d /usr/app/node_modules ]; then npm install; fi
#RUN npm run-script build-prod
#RUN ["chmod", "+x", "/usr/app/docker-entrypoint.sh"]
#ENTRYPOINT [ "/usr/app/docker-entrypoint.sh" ]
FROM nginx:stable
#COPY --from=builder /usr/app/dist/ /usr/share/nginx/html/
COPY source/dist/ /usr/share/nginx/html/